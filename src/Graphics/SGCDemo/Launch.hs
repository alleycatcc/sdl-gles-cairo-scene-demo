{-# LANGUAGE OverloadedStrings #-}

{-# LANGUAGE PackageImports #-}

module Graphics.SGCDemo.Launch ( launch, launchWithArgs ) where

import           Prelude hiding ( log, init )

import           Numeric ( readHex )
import           Data.Bits ( (.&.), shiftR )
import qualified Data.ByteString    as BS ( readFile )
import           Data.Text ( Text )
import qualified Data.StateVar      as STV ( get )
import           Data.List ( zip4, findIndex, zip5 )
import           Debug.Trace ( trace )
import           Data.Function ( (&) )
import           Data.Maybe ( isJust, fromJust )
import           Data.Either ( isLeft )
import           Data.ByteString    as BS ( ByteString )
import           Control.Applicative ( empty )
import           Data.Monoid ( (<>) )
import           Foreign ( Ptr
                         , free
                         , peekElemOff
                         , mallocArray )
import           Text.Printf ( printf )
import           Control.Monad ( when, unless, forM_, forM, join )
import           Data.Map as Dmap ( Map
                                  , toList )
import           Control.DeepSeq ( deepseq )
import qualified Data.Yaml as Y ( decodeEither' )
import qualified Data.Vector          as DV  ( toList )

import qualified Data.ByteString.Base64 as B64 ( decode )

import           Data.Stack ( stackNew )

import "matrix"  Data.Matrix        as DMX ( (!) )

import           Codec.Picture      as JP ( decodePng )

import           SDL as S
                 ( ($=)
                 , Mode (Normal)
                 , Profile (Compatibility, ES)
                 , initializeAll
                 , createWindow
                 , defaultWindow
                 , defaultOpenGL
                 , glColorPrecision
                 , glDepthPrecision
                 , glStencilPrecision
                 , glMultisampleSamples
                 , glProfile
                 , glCreateContext
                 , glSwapWindow )

import           Graphics.Rendering.OpenGL as GL
                 ( Color4 (Color4)
                   -- no constructors, just synonym for Int32
                 , GLsizei
                 , TextureObject (TextureObject)
                 , ShadingModel ( Smooth )
                 , PixelStoreDirection ( Unpack, Pack )
                 , ClearBuffer ( ColorBuffer, DepthBuffer )
                 , Vertex2 ( Vertex2 )
                 , Vertex3 ( Vertex3 )
                 , Vertex4 ( Vertex4 )
                 , PrimitiveMode ( Triangles, TriangleFan )
                 , PixelFormat ( DepthComponent )
                 , Position ( Position )

                  -- Not Ptr CUChar <that's the *Cairo* PixelData>
                 , PixelData ( PixelData )
                 , DataType ( Float )
                 , Capability ( Enabled, Disabled )
                 , TextureFilter ( Nearest )
                 , Size ( Size )
                 , GLfloat
                 , Vector3 ( Vector3 )
                 , Vector4 ( Vector4 )
                 , HintTarget (PolygonSmooth)
                 , HintMode (Nicest)
                 , BlendingFactor ( One, OneMinusSrcAlpha, SrcAlpha, SrcAlphaSaturate )
                 , PixelInternalFormat ( RGBA'
                                       , RGBA8
                                       , RGB8
                                       , RGB' -- GL_RGB
                                       )
                 , hint
                 , drawArrays
                 , genObjectNames
                 , viewport
                 , readPixels
                 , rowAlignment
                 , textureFilter
                 , blend
                 , blendFunc
                 , depthFunc
                 , clearColor )

import qualified Graphics.Rendering.OpenGL as GL
                 ( clear )

import           Linear ( V2 (..), V4 (..) )

import qualified Graphics.Rendering.Cairo as C
                 ( Render
                 , Operator (OperatorClear, OperatorSource)
                 -- only exported for certain cairo versions <see source>
                 -- formatStrideForWidth
                 , setOperator
                 , renderWith
                 , translate
                 , save
                 , restore
                 , transform
                 , rotate
                 , setLineWidth
                 , setSourceRGBA
                 , moveTo
                 , rectangle
                 , fill
                 , arc
                 , stroke )

-- sdl-cairo-demo-cat
import           Graphics.DemoCat.Render.Render     as SCC ( renderFrames )

-- mesh-obj-gles
import qualified Codec.MeshObjGles.Parse as Cmog
                 ( Config (Config)
                 , ConfigObjectSpec (ConfigObjectSpec)
                 , ConfigObjectSpecItem (ConfigObjectFilePath, ConfigObjectSource)
                 , ConfigMtlSpec (ConfigMtlFilePath, ConfigMtlSource)
                 , TextureConfig (TextureConfig)
                 , Sequence (Sequence)
                 , SequenceFrame (SequenceFrame)
                 , Texture (Texture)
                 , Burst (Burst)
                 , Vertices
                 , TexCoords
                 , TextureMap
                 , ObjName
                 , MtlName
                 , Normals
                 , Vertex2 (Vertex2)
                 , Vertex3 (Vertex3)
                 , TextureTypesSet
                 , TextureType
                 ( TextureDiffuse, TextureAmbient
                 , TextureDissolve, TextureSpecular
                 , TextureSpecularExp, TextureEmissive )
                 , materialTexture
                 , materialSpecularExp
                 , materialAmbientColor
                 , materialDiffuseColor
                 , materialSpecularColor
                 , materialTextureTypes
                 , makeInfiniteSequence
                 , tailSequence
                 , tcImageBase64
                 , tcWidth
                 , tcHeight
                 , parse )

import           Graphics.SGCDemo.Shader
                 ( initShaderMeshScene )

import           Graphics.SDLGles.GLES.Draw
                 ( pushColors
                 , rectangle
                 , rectangleTex
                 , triangle
                 , sphere
                 , cylinder
                 , cylinderTex
                 , circle
                 , coneSection
                 , coneSectionTex
                 , pushPositions
                 , pushPositionsWithArray
                 , pushTexCoords
                 , pushTexCoordsWithArray
                 , pushNormals
                 , pushNormalsWithArray
                 , pushAttributesVertex4
                 , pushAttributesFloat
                 , pushAttributesWithArrayVertex4
                 , pushAttributesWithArrayScalar
                 , rectangleStroke
                 )

import           Graphics.SDLGles.GLES.Coords
                 ( vec3
                 , verl3
                 , vec4
                 , vec3gld
                 , invertMajor'
                 , ver3
                 , ver3gld
                 , frustumF
                 , orthoF
                 , lookAtF
                 , normalize
                 , unProjectItF
                 , rotateX
                 , rotateY
                 , rotateZ
                 , scaleX
                 , scaleY
                 , scaleZ
                 , verple3
                 , invertMatrix
                 , identityMatrix
                 , multMatrices
                 , vecl4
                 , toMGC
                 , toMGCD
                 , translateX
                 , translateY
                 , translateZ
                 , ver3
                 , ver4
                 , vec3 )

import           Graphics.SDLGles.GLES.Shader
                 ( uniform
                 , useShader
                 , attrib )

import           Graphics.SDLGles.Util
                 ( appReplaceModel
                 , appUpdateMatrix
                 , appMultiplyModel
                 , appMultiplyRightModel
                 , appMultiplyView
                 , replaceModel
                 , replaceView
                 , replaceProj
                 , pushModel
                 , pushView
                 , pushProj
                 , stackPop'
                 , stackReplace' )

import           Graphics.SDLGles.Util2
                 ( frint )

import           Graphics.SDLGles.Util3
                 ( map3
                 , deg2rad
                 , vcross
                 , vmag
                 , v3x
                 , v3y
                 , v3z
                 , vdot
                 , vdiv
                 , float
                 , toDeg
                 , fst3
                 , snd3
                 , thd3 )

import           Graphics.SDLGles.GL.Util
                 ( wrapGL )

import           Graphics.SGCDemo.Util
                 ( hsvCycle
                 , nAtATime
                 , glTrueF
                 , glFalseF
                 , color
                 , col8
                 , color4
                 , color3
                 , inv
                 , randoms
                 )

import           Graphics.SDLGles.Texture
                 ( createTextures2DSimple
                 , activateTexture
                 , updateTexture
                 , updateTextures
                 , updateTextureCairo
                 , textureWithCairo
                 , textureNoCairo )

import           Graphics.SGCDemo.Config
                 ( doDebug
                 , isEmbedded
                 , useGLES
                 , mvpConfig )

import           Graphics.SDLGles.Config
                 ( defaultConfig
                 )

import           Graphics.SDLGles.Types
                 ( App (App)
                 , Log (Log, info, warn, err)
                 , Logger
                 , output565
                 , toShaderDC
                 , toShaderDT
                 , appConfig
                 , appLog
                 , appMatrix
                 , appUser
                 )

import qualified Graphics.SDLGles.Types as SDG
                 ( Config )

import           Graphics.SDLGles.Types
                 ( GraphicsData (GraphicsSingle, GraphicsSingleCairo, GraphicsMoving)
                 , ShaderD (ShaderDC, ShaderDT)
                 , Tex (NoTexture)
                 , GraphicsTextureMapping (GraphicsTextureMapping)
                 , ProjectionType (ProjectionFrustum, ProjectionOrtho)
                 , getCSurf
                 , graphicsTextureMappingGraphicsData
                 , graphicsTextureMappingTexture
                 , graphicsTextureMappingTextureObject
                 , mvpConfigProjectionType
                 , mvpConfigCubeScale
                 , mvpConfigTranslateZ
                 , attribAttribLocation
                 , uniformUniformLocation
                 , texWidth
                 , texHeight
                 , shaderDCProgram
                 , shaderDCUniformModel
                 , shaderDCUniformView
                 , shaderDCUniformProjection
                 , shaderDCAttributePosition
                 , shaderDCAttributeColors
                 , shaderDTProgram
                 , shaderDTUniformModel
                 , shaderDTUniformView
                 , shaderDTUniformProjection
                 , shaderDTUniformTexture
                 , shaderDTAttributePosition
                 , shaderDTAttributeTexCoord )

import qualified Graphics.SGCDemo.Scene as GsgcScene ( sceneObj, sceneMtl )

import           Graphics.SGCDemo.Types
                 ( App'
                 , Config
                 , Shader' (Shader'C, Shader'T)
                 , SGCAppUserData (SGCAppUserData)
                 , Buffer (Buffer)
                 , BufferMaker (MakeBufferScalar, MakeBufferVertex3, MakeBufferVertex4)
                 , MainLoop (MainLoop)
                 , matrixVarsModel
                 , matrixVarsView
                 , matrixVarsProjection
                 , mainLoopConfig
                 , mainLoopApp'
                 , mainLoopShaders
                 , mainLoopBufferMb
                 , mainLoopTexMaps
                 , mainLoopMesh
                 , mainLoopT
                 , mainLoopRands
                 , mainLoopModelRx
                 , mainLoopModelRy
                 , mainLoopModelTz
                 , mainLoopArgs
                 , shaderVarsCAp
                 , shaderVarsCAc
                 , shaderVarsCAn
                 , shaderVarsTAp
                 , shaderVarsTAtc
                 , shaderVarsTAn
                 , shaderVarsTUt
                 , shaderVarsTUtim
                 , shaderVarsTUdvo
                 , shaderVarsTUdofog
                 , shaderVarsMAp
                 , shaderVarsMAtc
                 , shaderVarsMAn
                 , shaderVarsMUse
                 , shaderVarsMUac
                 , shaderVarsMUdc
                 , shaderVarsMUsc
                 , shaderVarsMUt
                 , shaderVarsMUas
                 , shaderVarsMUss
                 , shaderVarsMUtim
                 , shaderVarsMSAp
                 , shaderVarsMSAtc
                 , shaderVarsMSAn
                 , shaderVarsMSUse
                 , shaderVarsMSUdc
                 , shaderVarsMSUsc
                 , shaderVarsMSUt
                 , bufferLength
                 , bufferAry
                 , app'Config
                 , sgcAppConfig
                 , configViewportWidth
                 , configViewportHeight
                 , configSceneScale
                 , shaderMatrix
                 , shaderProgram
                 , shaderShaderVars
                 )
import           Graphics.SDLGles.SDL.Events ( processEvents )
import           Graphics.SDLGles.App ( init, runLoop )

mouseWheelFactor = 1 / 4.0

meshSpec' :: Log -> SDG.Config -> Cmog.TextureMap -> IO [(Tex, GraphicsData)]
meshSpec' log sdlGlesConfig textureMap = meshSpecGraphicsDecode log sdlGlesConfig textures' where
   tex' config' = (Cmog.tcWidth config', Cmog.tcHeight config', Cmog.tcImageBase64 config')
   textures' = map tex' textureConfigs'
   textureConfigs' = values textureMap

meshSpec app sdlGlesConfig textureMap = spec' >>= mapM map' where
    log = appLog app
    spec' = meshSpec' log sdlGlesConfig textureMap
    map' (tex, graphics) = GraphicsTextureMapping graphics tex <$> newTexture
    newTexture = head <$> createTextures2DSimple app 1

meshSpecGraphicsDecode :: Log -> SDG.Config -> [(Int, Int, ByteString)] -> IO [(Tex, GraphicsData)]
meshSpecGraphicsDecode log sdlGlesConfig spec = do
    let w   =  fst3
        h   =  snd3
        img =  thd3
        gs' :: IO [GraphicsData]
        gs' =  forM spec  $ \spec' -> do
            img' <- decodeImage' log . img $ spec'
            pure $ GraphicsSingle img' True
    ts      <- forM spec $ \spec' -> textureNoCairo sdlGlesConfig (w spec') (h spec')
    gs <- gs'
    pure $ zip ts gs

-- xxx
-- dimension = 0.5

launch :: (Logger, Logger, Logger) -> Maybe (Int, Int) -> IO ()
launch x y = launch' x y []

launchWithArgs :: (Logger, Logger, Logger) -> Maybe (Int, Int) -> [String] -> IO ()
launchWithArgs = launch'

launch' :: (Logger, Logger, Logger) -> Maybe (Int, Int) -> [String] -> IO ()
launch' loggers@(info', warn', error') dimsMb args = do
    let log = Log info' warn' error'
        debug' = debug log

    configYaml <- if isEmbedded then pure configYamlInline
                                else BS.readFile "config.yaml"

    config <- do  let  error' err = die log $ "Couldn't decode config.yaml: " ++ show err
                  either error' pure $ Y.decodeEither' configYaml

    app <- init loggers sdlGlesConfig (SGCAppUserData config) mvpConfig args

    let dims' Nothing = do  info' $ "Getting width & height from config"
                            pure (configViewportWidth config, configViewportHeight config)
        dims' (Just (width', height')) = do
                            info' $ "Got width and height from caller"
                            pure (width', height')
    (viewportWidth, viewportHeight) <- dims' dimsMb
    info' $ printf "Viewport dims: %s x %s" (show viewportWidth) (show viewportHeight)

    (_, sceneSeqMb') <- do
        -- no textures in this demo.
        let textureConfigYaml' = Nothing
            -- xxx will not work on mobile
            mtlObjSpec' = sequence2 (GsgcScene.sceneObj, GsgcScene.sceneMtl)
        initMeshes app textureConfigYaml' mtlObjSpec' =<< (Just <$> getMeshLimitFrames app)

    let getSceneBuffers' = do  let Cmog.Sequence frames = fromJust sceneSeqMb'
                                   frame = head frames
                                   Cmog.SequenceFrame bursts = frame
                               initBuffers bursts [ MakeBufferVertex3 -- pos
                                                  , MakeBufferVertex4 -- tc
                                                  , MakeBufferVertex4 -- normals
                                                  ]
    sceneBuffersMb' <- Just <$> getSceneBuffers'

    rands <- randoms

    debug' "initShaders"
    meshShaderScene <- initShaders log

    debug' "starting loop"

    let mesh = ( Cmog.makeInfiniteSequence <$> sceneSeqMb' )

    runLoop app appLoop $ MainLoop
        config app
        meshShaderScene
        sceneBuffersMb' ()
        mesh
        0 -- modelRx
        0 -- modelRy
        0 -- cameraZ
        0 -- t
        rands args

appLoop curLoop = do
    let log = appLog app
        info' = info log
        debug' = debug log

        sceneSeqMb' = mesh

        sceneScale = config  & configSceneScale

        config     = curLoop & mainLoopConfig
        app        = curLoop & mainLoopApp'
        shaders    = curLoop & mainLoopShaders
        buffersMb  = curLoop & mainLoopBufferMb
        mesh       = curLoop & mainLoopMesh
        modelRx    = curLoop & mainLoopModelRx
        modelRy    = curLoop & mainLoopModelRx
        modelTz    = curLoop & mainLoopModelTz
        t          = curLoop & mainLoopT
        rands      = curLoop & mainLoopRands
        args       = curLoop & mainLoopArgs

    debug' "* looping"

    -- debug' "updating textures"
    -- textureMappingsCube' <- updateTextures app textureMappingCube

    let viewportWidth = configViewportWidth $ config
        viewportHeight = configViewportHeight $ config
    ( qPressed, click, dragAmounts, wheelOrPinchAmount ) <- processEvents log ( viewportWidth, viewportHeight )

    let (rx', ry', rotationsMouse') = rotationsForDrag dragAmounts

    let modelZDelta'
          | isJust wheelOrPinchAmount = (* mouseWheelFactor) . frint . fromJust $ wheelOrPinchAmount
          | otherwise = 0

    let modelRx' = modelRx + rx'
        modelRy' = modelRy + ry'
        modelTz' = modelTz + modelZDelta'

    -- 'multiply right': for pushing the model away from yourself in grand,
    -- fixed system
    -- 'multiply': for relativising yourself and drawing locally

    app' <- do  let model' = multMatrices [rotationsMouse', translateZ modelZDelta']
                pure $ app & appMultiplyRightModel model'

    debug' $ printf "wheel or pinch: %s" (show wheelOrPinchAmount)
    debug' $ printf "dragAmounts: %s" (show dragAmounts)

    info' $ printf "rotations: x %.1f y %.1f" modelRx' modelRy'
    info' $ printf "dz: %.1f" modelTz'

    wrapGL log "clear" $ GL.clear [ColorBuffer, DepthBuffer]

    let meshShaderScene  = shaders

    let sceneSeq = fromJust sceneSeqMb'
        sceneBuf = fromJust buffersMb
    drawScene app' sceneSeq sceneScale meshShaderScene sceneBuf t args

    let newLoop = curLoop { mainLoopT       = t + 1
                          , mainLoopApp'    = app'
                          , mainLoopMesh    = Cmog.tailSequence <$> sceneSeqMb'
                          , mainLoopRands   = tail rands
                          , mainLoopModelRx = modelRx'
                          , mainLoopModelRy = modelRy'
                          , mainLoopModelTz = modelTz'
                          , mainLoopTexMaps = () }

        continue | qPressed = Nothing
                 | otherwise = Just newLoop

    pure continue

debug | doDebug   = info
      | otherwise = const . const . pure $ ()

toRight (Right x) = x
toRight _         = error "not Right"
toLeft (Left x)   = x
toLeft _          = error "not Left"

-- (arbitrary choice of whether x is first or y, but these are small amounts
-- so it shouldn't matter too much)
rotationsForDrag (Just (x, y)) = let rx' = frint y
                                     ry' = frint x
                                 in  (rx', ry', multMatrices [ rotateX rx', rotateY ry' ])
rotationsForDrag Nothing       = (0, 0, identityMatrix)

decodeImage imageBase64 = do
    let imageBase64Decoded = B64.decode imageBase64
    (when . isLeft $ imageBase64Decoded) $
        err' . toLeft $ imageBase64Decoded
    let imageBase64Decoded' = toRight imageBase64Decoded
        dynamicImage = decodePng imageBase64Decoded'

    (when . isLeft $ dynamicImage) $
        err'' . toLeft $ dynamicImage
    dynamicImage where
        err' e = Left $ "Couldn't decode base64, " ++ e
        err'' e = Left $ "Couldn't decode png, " ++ e

decodeImage' log imageBase64 =
    let img = decodeImage imageBase64 in
    if isLeft img then die log "Bad image"
                  else pure $ toRight img

initShaders log = initShaderMeshScene log

initMeshesParse :: Log -> Maybe ByteString -> IO ([Text], ByteString) -> IO (Either String (Cmog.Sequence, Cmog.TextureMap))
initMeshesParse log textureConfigYamlMb meshSpec = do
    (obj', mtl') <- meshSpec
    let objSources' = map Cmog.ConfigObjectSource obj'
    let meshConfig = Cmog.Config c1 c2 c3
        c1 = Cmog.ConfigObjectSpec objSources'
        c2 = Cmog.ConfigMtlSource mtl'
        c3 = textureConfigYamlMb
    info log "parsing"
    Cmog.parse meshConfig

initSceneParse :: Log -> Maybe ByteString -> IO (Either String (Cmog.Sequence, Cmog.TextureMap))
initSceneParse log textureConfigYamlMb = do
    -- xxx mobile
    mtlSrc' <- GsgcScene.sceneMtl
    sceneObj' <- GsgcScene.sceneObj
    let objSources' = map Cmog.ConfigObjectSource sceneObj'
        sceneConfig = Cmog.Config c1 c2 c3
        c1 = Cmog.ConfigObjectSpec objSources'
        c2 = Cmog.ConfigMtlSource mtlSrc'
        c3 = textureConfigYamlMb
    info log "parsing"
    Cmog.parse sceneConfig

drawScene app sceneSeq sceneScale shader buffers t args = do
    let log = appLog app
        prog       = shaderProgram shader
        matrixVars = shaderMatrix shader
        umm        = matrixVars & uniformUniformLocation . matrixVarsModel
        umv        = matrixVars & uniformUniformLocation . matrixVarsView
        ump        = matrixVars & uniformUniformLocation . matrixVarsProjection
        Cmog.Sequence frames' = sceneSeq

    let
        -- infinite list => head is fine.
        frame' = head frames'
        Cmog.SequenceFrame bursts' = frame'

    useShader log prog

    let _app = app & appMultiplyModel model'
        appmatrix = appMatrix _app
        scale' = sceneScale
        model' = multMatrices [ scaleX scale'
                              , scaleY scale'
                              , scaleZ scale' ]
        (model, view, proj) = map3 stackPop' appmatrix

    uniform log "model" umm =<< toMGC model
    uniform log "view" umv =<< toMGC view
    uniform log "proj" ump =<< toMGC proj

    let reducer acc burst =
            let Cmog.Burst vertices' texCoordsMb' normalsMb' material' = burst
            in  acc + length vertices'

    let burstsX' = zip bursts' buffers
        doPush = t == 0
    forM_ burstsX' $ \(burst, burstBuffer') ->
        drawSceneBurst log shader Nothing burstBuffer' burst doPush

    pure ()

drawSceneBurst log shader textureObjMb buffers burst doPush = do
    let shaderVars = shaderShaderVars shader
        ap  = shaderVars & attribAttribLocation   . shaderVarsMSAp
        atc = shaderVars & attribAttribLocation   . shaderVarsMSAtc
        an  = shaderVars & attribAttribLocation   . shaderVarsMSAn
        use = shaderVars & uniformUniformLocation . shaderVarsMSUse
        udc = shaderVars & uniformUniformLocation . shaderVarsMSUdc
        usc = shaderVars & uniformUniformLocation . shaderVarsMSUsc
        utt = shaderVars & uniformUniformLocation . shaderVarsMSUt

        Cmog.Burst vertices' texCoordsMb' normalsMb' material' = burst

        vert' = map cmogVertex3ToLocalVertex3 . DV.toList $ vertices'
        toNormalsMb = map (cmogVertex3ToLocalVertex4 1.0) . DV.toList

        [ vBuf, tcBuf, nBuf ] = buffers

        vAry  = bufferAry vBuf
        nAry  = bufferAry nBuf

        normals'   :: [Vertex4 Float]
        normals'   = maybe none' toNormalsMb normalsMb'
        none' = []
        len' = length vert'

        specularExp'   = Cmog.materialSpecularExp material' -- Float
        diffuseColor'  = cmogVertex3ToLocalVertex4 1.0 $ Cmog.materialDiffuseColor material'
        specularColor' = cmogVertex3ToLocalVertex4 1.0 $ Cmog.materialSpecularColor material'

    -- activateTextureMaybe log utt textureObjMb

    uniform log "specular exp"  use specularExp'
    uniform log "diffuse color"  udc diffuseColor'
    uniform log "specular color"  usc specularColor'

    if doPush then do
        pushPositionsWithArray log vAry ap (Just vert')
        pushNormalsWithArray log nAry an (Just normals')
    else do
        pushPositionsWithArray log vAry ap Nothing
        pushNormalsWithArray log nAry an Nothing

    attrib log "ap" ap Enabled
    attrib log "an" an Enabled

    wrapGL log "drawArrays" . drawArrays Triangles 0 . frint $ len'

    attrib log "an" an Disabled
    attrib log "ap" ap Disabled

    pure ()

values :: Dmap.Map k v -> [v]
values = map snd . Dmap.toList

fs `asterisk` x = map map' fs where map' f = f x

configYamlInline :: ByteString
configYamlInline =
    "viewportWidth: 480\n" <>
    "viewportHeight: 793\n" <>
    "sceneScale: 0.1\n"

-- 1 for now in this scene
getMeshLimitFrames app = do
    let log = appLog app
        sdlGlesConfig = appConfig app
        config = app'Config app
    debug log $ "going to force deepseq with all frames"
    pure id

initMeshes app textureConfigYaml' mtlObjSpec limitFramesMb = do
    let limitFrames = fromJust limitFramesMb
    let log = appLog app
        sdlGlesConfig = appConfig app
        config = app'Config app
    info log "Please wait . . . (parsing obj files, will take a while)"
    parsedEi' <- initMeshesParse log textureConfigYaml' mtlObjSpec
    info log "Done parsing"
    when (isLeft parsedEi') .
        dieM log $ printf "Unable to parse obj file: %s" (toLeft parsedEi')

    let (seq', texMap') = toRight parsedEi'
        meshSpec' = meshSpec app sdlGlesConfig texMap'
        no' = meshSpec' >>= \spec' -> pure (spec', seq')
        yes' limitFrames = do
            let Cmog.Sequence seqFrames' = seq'
                seq'' = Cmog.Sequence . limitFrames $ seqFrames'
            info log "getting it"
            spec' <- deepseq seq'' meshSpec'
            info log "gotting it"
            debug log "done forcing deepseq"
            pure (spec', seq'')

    (spec', seq'') <- maybe no' yes' limitFramesMb
    pure (spec', Just seq'')

    -- deepseq in combination with our limitFrames
    -- function works correctly: it only forces the
    -- chunk which results from limitFrames.
    -- info log $ "spec' length: " ++ length spec'
    -- pure (spec', Just seq')

initBuffers :: [Cmog.Burst] -> [BufferMaker] -> IO [[Buffer]]
initBuffers bursts attributeConstructors = do
    let print' burst = do
            let (vl, tcl) = analyzeBurst burst
            putStrLn $ printf "vertex: %d, tex: %d, norm: %d" vl tcl vl
    when True $ mapM_ print' bursts

    mapM (makeBuffers attributeConstructors) bursts

-- position (num vertices)
-- texcoords (num texcoords)
-- normals (num vertices)
-- and all other attributes are tied to num vertices.

makeBuffers attributeConstructors burst = do
    let (vl, tcl) = analyzeBurst burst
        map' (len', maker') = makeBuffer len' maker'
        x = vl : tcl : repeat vl

    mapM map' $ zip x attributeConstructors

makeBuffer len maker = Buffer len' <$> mallocArray len' where
    len' = len * factor'
    factor' = case maker of MakeBufferScalar -> 1
                            MakeBufferVertex3 -> 3
                            MakeBufferVertex4 -> 4

-- assumption: it is possible that there are fewer texture coordinates
-- entries than vertices, but always an equal number of normals.
analyzeBurst burst = (vertexLength, texCoordLength) where
    Cmog.Burst vertices' texCoordsMb' normalsMb' material' = burst
    vertexLength   = numVertices'
    texCoordLength = length (maybe [] DV.toList texCoordsMb')
    numVertices'   = length vertices'

die log str = do
    _ <- err log str
    error str

dieM log str = err log str >> empty

cmogVertex3ToLocalVertex3     (Cmog.Vertex3 a b c) = Vertex3 a b c
cmogVertex2ToLocalVertex4 c d (Cmog.Vertex2 a b  ) = Vertex4 a b c d
cmogVertex3ToLocalVertex4 d   (Cmog.Vertex3 a b c) = Vertex4 a b c d

sdlGlesConfig = defaultConfig

sequence2 (a, b) = tuple2 <$> a <*> b
tuple2 a b = (a, b)
