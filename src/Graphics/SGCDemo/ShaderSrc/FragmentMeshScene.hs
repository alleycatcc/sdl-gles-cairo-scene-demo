module Graphics.SGCDemo.ShaderSrc.FragmentMeshScene ( shaderSrc
                                                    ) where

shaderSrc =
    "#version 100\n" ++
    "#ifdef GL_ES\n" ++
    "precision mediump float;\n" ++
    "#endif\n" ++
    "\n" ++
    "// uniform sampler2D texture;\n" ++
    "\n" ++
    "uniform float specularExp;\n" ++
    "uniform vec4 diffuseColor;\n" ++
    "uniform vec4 specularColor;\n" ++
    "\n" ++
    "varying vec4 v_position;\n" ++
    "// varying vec2 v_texcoord;\n" ++
    "varying vec4 v_normal;\n" ++
    "\n" ++
    "float specularStrength = 10.0;\n" ++
    "\n" ++
    "vec4 viewPos  = vec4 (-0.0, -0.0, 10.0, 1.0);\n" ++
    "\n" ++
    "struct light {\n" ++
    "    float specularStrength;\n" ++
    "    float specularExp;\n" ++
    "    vec4 diffuseColor;\n" ++
    "    vec4 specularColor;\n" ++
    "    vec4 lightPos;\n" ++
    "};\n" ++
    "\n" ++
    "// ok to send entire struct as an arg?\n" ++
    "\n" ++
    "vec4 get_lighting (vec4 viewDir, vec4 norm, light l)\n" ++
    "{\n" ++
    "    vec4 lightDir = normalize (l.lightPos - v_position);\n" ++
    "    float lightProj = dot (norm, lightDir);\n" ++
    "\n" ++
    "    // xxx\n" ++
    "    lightProj = abs (lightProj);\n" ++
    "\n" ++
    "    vec4 diffuse = max (lightProj, 0.0) * l.diffuseColor;\n" ++
    "    diffuse.w = l.diffuseColor.w;\n" ++
    "\n" ++
    "    vec4 reflectDir = reflect (-lightDir, norm);\n" ++
    "    float reflectProj = dot (viewDir, reflectDir);\n" ++
    "    float spec = pow (max (reflectProj, 0.0), l.specularExp);\n" ++
    "    vec4 specular = l.specularStrength * l.specularColor * spec;\n" ++
    "\n" ++
    "    vec4 ambient = vec4 (1.0, 1.0, 1.0, 1.0);\n" ++
    "\n" ++
    "    return ambient + specular + diffuse;\n" ++
    "}\n" ++
    "\n" ++
    "void main()\n" ++
    "{\n" ++
    "    light l = light (\n" ++
    "        specularStrength,\n" ++
    "        specularExp,\n" ++
    "        diffuseColor,\n" ++
    "        specularColor,\n" ++
    "        vec4 (-10.0, -2.0, 3.0, 1.0)\n" ++
    "    );\n" ++
    "\n" ++
    "    // vec4 init = texture2D (texture, v_texcoord);\n" ++
    "    vec4 init = diffuseColor;\n" ++
    "\n" ++
    "    vec4 norm = normalize (v_normal);\n" ++
    "    norm.w = 0.0;\n" ++
    "\n" ++
    "    vec4 viewDir = normalize (viewPos - v_position);\n" ++
    "\n" ++
    "    vec4 lightTotal = vec4 (0.0, 0.0, 0.0, 0.0);\n" ++
    "    lightTotal += get_lighting (viewDir, norm, l);\n" ++
    "\n" ++
    "    gl_FragColor = init * lightTotal;\n" ++
    "\n" ++
    "    gl_FragColor.w = 0.9;\n" ++
    "}\n"

