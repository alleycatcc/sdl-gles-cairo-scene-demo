{-# LANGUAGE PackageImports #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}

module Graphics.SGCDemo.Types
    ( App'
    , MainLoop (MainLoop)
    , SGCAppUserData (SGCAppUserData)
    , Config
    , Color
    , Shader' (Shader'C, Shader'T, Shader'M)
    , ShaderVars (ShaderVarsC, ShaderVarsT, ShaderVarsM, ShaderVarsMS)
    , Buffer (Buffer)
    , BufferMaker (MakeBufferScalar, MakeBufferVertex3, MakeBufferVertex4)
    , app'Config
    , sgcAppConfig
    , mkMatrix
    , shaderMatrix
    , shaderProgram
    , shaderShaderVars
    , mkShaderVarsC
    , mkShaderVarsT
    , mkShaderVarsM
    , mkShaderVarsMS
    , configSceneScale
    , configViewportWidth
    , configViewportHeight
    , mainLoopConfig
    , mainLoopApp'
    , mainLoopShaders
    , mainLoopBufferMb
    , mainLoopTexMaps
    , mainLoopMesh
    , mainLoopModelRx
    , mainLoopModelRy
    , mainLoopModelTz
    , mainLoopT
    , mainLoopRands
    , mainLoopArgs
    , bufferLength
    , bufferAry
    , matrixVarsModel
    , matrixVarsView
    , matrixVarsProjection
    , shaderVarsCAp
    , shaderVarsCAc
    , shaderVarsCAn
    , shaderVarsTAp
    , shaderVarsTAtc
    , shaderVarsTAn
    , shaderVarsTUt
    , shaderVarsTUtim
    , shaderVarsTUdvo
    , shaderVarsTUdofog
    , shaderVarsMAp
    , shaderVarsMAtc
    , shaderVarsMAn
    , shaderVarsMUse
    , shaderVarsMUac
    , shaderVarsMUdc
    , shaderVarsMUsc
    , shaderVarsMUt
    , shaderVarsMUas
    , shaderVarsMUss
    , shaderVarsMUtim
    , shaderVarsMSAp
    , shaderVarsMSAtc
    , shaderVarsMSAn
    , shaderVarsMSUse
    , shaderVarsMSUdc
    , shaderVarsMSUsc
    , shaderVarsMSUt
    ) where

import           Foreign.C.Types ( CUChar, CUShort )
import           Control.Applicative ( (<|>) )
import qualified Data.StateVar        as STV ( get )
import           Data.Monoid ( (<>) )

import           Data.Yaml as Y
                 ( (.:)
                 , FromJSON
                 , parseJSON )

import qualified Data.Yaml as Y
                 ( Value (Object) )

import "matrix"  Data.Matrix        as DMX ( Matrix )
import           Data.Stack ( Stack )
import           Graphics.Rendering.OpenGL as GL
                 ( PixelData ( PixelData )
                 , GLmatrix
                 , GLfloat
                 , GLdouble
                 , Vertex3
                 , Vertex4
                 , Program
                 , UniformLocation
                 , AttribLocation
                 , PixelFormat ( RGB, RGBA )
                 , DataType ( UnsignedByte, UnsignedShort565 )
                 , TextureObject
                 , attribLocation
                 , uniformLocation
                 )

import           Foreign
                 ( Ptr, mallocArray )

-- impl?
import           Graphics.SDLGles.GL.Util
                 ( wrapGL
                 )

import           Graphics.SDLGles.Types
                 ( App
                 , Log
                 , GraphicsTextureMapping
                 , Shader (Shader)
                 , Attrib (Attrib)
                 , Uniform (Uniform)
                 , MatrixVarsClass
                   (matrixVarsModel, matrixVarsView, matrixVarsProjection)
                 , ShaderVarsClass
                   ( shaderVarsCAp, shaderVarsCAc, shaderVarsCAn
                   , shaderVarsTAp, shaderVarsTAtc, shaderVarsTAn
                   , shaderVarsTUt )
                 , appUser
                 , attribName
                 , attribAttribLocation
                 , uniformName
                 , uniformUniformLocation
                 , shader'Program
                              , shader'ShaderVars
                              , shader'Matrix
                 )

import           Graphics.SDLGles.Util
                 ( liftA2, liftA3, liftA4, liftA5, liftA6
                 , liftA7, liftA8, liftA9, liftA10, liftA11 )

-- mesh-obj-gles
import qualified Codec.MeshObjGles.Parse as Cmog
                 ( Sequence (Sequence) )

data SGCAppUserData = SGCAppUserData { sgcAppConfig :: Config }

type App' = App SGCAppUserData

app'Config :: App' -> Config
app'Config = sgcAppConfig . appUser

-- | these are provided as type parameters to `SGLGles.Shader`.
-- • while we don't expect MatrixVars to change, this does allow some flexibility.

data MatrixVars = MatrixVars { matrixVarsModel'      :: Uniform
                             , matrixVarsView'       :: Uniform
                             , matrixVarsProjection' :: Uniform }
                             deriving (Show, Eq)

data ShaderVars = ShaderVarsC  { shaderVarsCAp'    :: Attrib
                               , shaderVarsCAc'    :: Attrib
                               , shaderVarsCAn'    :: Attrib }
                | ShaderVarsT  { shaderVarsTAp'    :: Attrib
                               , shaderVarsTAtc'   :: Attrib
                               , shaderVarsTAn'    :: Attrib
                               , shaderVarsTUt'    :: Uniform
                               , shaderVarsTUtim   :: Uniform
                               , shaderVarsTUdvo   :: Uniform
                               , shaderVarsTUdofog :: Uniform }
                | ShaderVarsM  { shaderVarsMAp     :: Attrib
                               , shaderVarsMAtc    :: Attrib
                               , shaderVarsMAn     :: Attrib
                               , shaderVarsMUse    :: Uniform
                               , shaderVarsMUac    :: Uniform
                               , shaderVarsMUdc    :: Uniform
                               , shaderVarsMUsc    :: Uniform
                               , shaderVarsMUt     :: Uniform
                               , shaderVarsMUas    :: Uniform
                               , shaderVarsMUss    :: Uniform
                               , shaderVarsMUtim   :: Uniform }
                | ShaderVarsMS { shaderVarsMSAp    :: Attrib
                               , shaderVarsMSAtc   :: Attrib
                               , shaderVarsMSAn    :: Attrib
                               , shaderVarsMSUse   :: Uniform
                               , shaderVarsMSUdc   :: Uniform
                               , shaderVarsMSUsc   :: Uniform
                               , shaderVarsMSUt    :: Uniform }
                  deriving (Eq, Show)

instance MatrixVarsClass MatrixVars where
    matrixVarsModel      = matrixVarsModel'
    matrixVarsView       = matrixVarsView'
    matrixVarsProjection = matrixVarsProjection'

instance ShaderVarsClass ShaderVars where
    shaderVarsCAp        = shaderVarsCAp'
    shaderVarsCAc        = shaderVarsCAc'
    shaderVarsCAn        = shaderVarsCAn'
    shaderVarsTAp        = shaderVarsTAp'
    shaderVarsTAtc       = shaderVarsTAtc'
    shaderVarsTAn        = shaderVarsTAn'
    shaderVarsTUt        = shaderVarsTUt'

mkAttribute log prog str = liftA2 Attrib name loc where
    name = pure str
    loc = (wrapGL log ("attribLocation "  <> str) . STV.get $ attribLocation  prog str)

mkUniform log prog str   = liftA2 Uniform name loc where
    name = pure str
    loc = (wrapGL log ("uniformLocation "  <> str) . STV.get $ uniformLocation  prog str)

mkMatrix log prog =
    liftA3 MatrixVars a b c where
        a = unif' "model"
        b = unif' "view"
        c = unif' "projection"
        unif' = mkUniform log prog

mkShaderVarsC log prog =
    liftA3 ShaderVarsC a b c where
        a = att' "a_position"
        b = att' "a_color"
        c = att' "a_normal"
        att' = mkAttribute log prog

mkShaderVarsT log prog =
    liftA7 ShaderVarsT a b c d e f g where
        a = att' "a_position"
        b = att' "a_texcoord"
        c = att' "a_normal"
        d = unif' "texture"
        e = unif' "transpose_inverse_model"
        f = unif' "do_vary_opacity"
        g = unif' "do_fog"
        att' = mkAttribute log prog
        unif' = mkUniform log prog

mkShaderVarsM log prog =
    liftA11 ShaderVarsM a b c d e f g h i j k where
        a = att' "a_position"
        b = att' "a_texcoord"
        c = att' "a_normal"
        d = unif' "specularExp"
        e = unif' "ambientColor"
        f = unif' "diffuseColor"
        g = unif' "specularColor"
        h = unif' "texture"
        i = unif' "ambientStrength"
        j = unif' "specularStrength"
        k = unif' "transpose_inverse_model"
        att' = mkAttribute log prog
        unif' = mkUniform log prog

mkShaderVarsMS log prog =
    liftA7 ShaderVarsMS a b c d e f g where
        a = att' "a_position"
        b = att' "a_texcoord"
        c = att' "a_normal"
        d = unif' "specularExp"
        e = unif' "diffuseColor"
        f = unif' "specularColor"
        g = unif' "texture"
        att' = mkAttribute log prog
        unif' = mkUniform log prog

data Shader' = Shader'C (Shader MatrixVars ShaderVars)
             | Shader'T (Shader MatrixVars ShaderVars)
             | Shader'M (Shader MatrixVars ShaderVars)
              deriving (Eq, Show)

shader' (Shader'C x) = x
shader' (Shader'T x) = x
shader' (Shader'M x) = x

shaderProgram    = shader'Program    . shader'
shaderShaderVars = shader'ShaderVars . shader'
shaderMatrix     = shader'Matrix     . shader'

type Color = (Float, Float, Float, Float)

-- | these contain the image data (in JuicyPixels form, not to be confused
--   with the backing array) and optionally the cairo frames and movie
--   timings.
-- • they get coupled to a Tex (our structure which holds the texture
--   arrays) and a TextureObject (GL's "texture name" integer).

-- | we use VertexX (not VectorX), also for the normal, because Vector just
--   makes it more complicated with no benefit.

data Config = Config { configViewportWidth :: Int
                     , configViewportHeight :: Int
                     , configSceneScale :: Float
                     }

instance FromJSON Config where
    parseJSON (Y.Object v) = Config
        <$> v .: "viewportWidth"
        <*> v .: "viewportHeight"
        <*> v .: "sceneScale"
    parseJSON _ = error "invalid type for parseJSON Config"

data MainLoop = MainLoop { mainLoopConfig :: Config
                         , mainLoopApp' :: App'
                         , mainLoopShaders :: Shader'
                         , mainLoopBufferMb :: Maybe [[Buffer]]
                         , mainLoopTexMaps :: ()
                         , mainLoopMesh :: Maybe Cmog.Sequence
                         , mainLoopModelRx :: Float
                         , mainLoopModelRy :: Float
                         , mainLoopModelTz :: Float
                         , mainLoopT :: Int
                         , mainLoopRands :: [Double]
                         , mainLoopArgs :: [String] }

-- | in the end the backing array is always a Ptr Float of course.
-- • we use 3 for verts, 4 for texcoords and normals.
data Buffer = Buffer { bufferLength :: Int
                     , bufferAry    :: Ptr Float }

data BufferMaker = MakeBufferScalar
                 | MakeBufferVertex3
                 | MakeBufferVertex4
