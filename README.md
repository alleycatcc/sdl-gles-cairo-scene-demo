# Proof-of-concept SDL/Cairo/OpenGL app framework in Haskell for Android & desktop.
## Also demonstrates how to build Haskell mobile (Android) apps using [ghc-cross-compile](https://gitlab.com/alleycatcc/ghc-cross-compile) and [haskell-android-sdl](https://gitlab.com/alleycatcc/haskell-android-sdl).

----

## To use:

- Build the .apk

        cd <some-project-dir>
        git clone https://gitlab.com/alleycatcc/mesh-obj-gles.git
        git clone https://gitlab.com/alleycatcc/sdl-cairo-demo-cat.git
        git clone https://gitlab.com/alleycatcc/sdl-gles-cairo-demo.git
        for i in mesh-obj-gles sdl-cairo-demo-cat sdl-gles-cairo-demo; do
            cd "$i"; git submodule update --init --recursive; cd ..
        done
        cd sdl-gles-cairo-demo
        bin/docker
    
- Connect the device via USB. Make sure it's in 'developer mode'.
- Push the .apk

        docker run --privileged -v /dev/bus/usb:/dev/bus/usb -it sdl-gles-android:latest
    

    - If the device shows an ok/cancel dialog try to accept immediately.
    