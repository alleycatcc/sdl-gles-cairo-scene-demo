FROM alleycatcc/haskell-android-sdl:latest

COPY usersrc.tar /
RUN mkdir plugin && cd plugin && tar xvf /usersrc.tar

COPY vars-plugin.sh-docker haskell-android-sdl/vars-plugin.sh

# COPY Hslib.hs haskell-android-sdl/hs-lib/src

RUN cd haskell-android-sdl && bin/build-project hs-build-user

COPY Hslib.hs haskell-android-sdl/hs-lib/src
COPY main.c haskell-android-sdl/src
COPY SDLActivity.java haskell-android-sdl/src
COPY AndroidManifest.xml haskell-android-sdl/build

# @temp
COPY vars-plugin.sh haskell-android-sdl/vars-plugin.sh

RUN cd haskell-android-sdl && bin/build-project prepare-build

RUN cd haskell-android-sdl && bin/build-project main-build

# right place for this?
COPY AndroidManifest.xml haskell-android-sdl/build

CMD cd haskell-android-sdl && bin/build-project apk
