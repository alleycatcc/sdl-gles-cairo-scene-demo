acat_plugin_haskellpackagesuser="
    cabal /plugin/sdl-cairo-demo-cat
    cabal /plugin/mesh-obj-gles
    cabal /plugin/sdl-gles-cairo-demo
"

# --- the appname: increment the number at the end to make a new app.
acat_plugin_appname=cc.alleycat.sdlglesscene1
# --- the icon text in the apps menu.
acat_plugin_appnametext=AlleyScene
